package patientsPage;

import data.BaseData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.Patient;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatientsPage {
    public ListView<String> lvPatients;
    public Button btnAddPatient, btnDltPatient;

    BaseData baseData = BaseData.getBaseData();
    String selectedItem = "";

    public void initialize() throws SQLException {
        ObservableList<String> doctorObservableList = FXCollections.observableArrayList(createItemLabel());
        lvPatients.setItems(doctorObservableList);
        MultipleSelectionModel<String> langsSelectionModel = lvPatients.getSelectionModel();
        langsSelectionModel.selectedItemProperty().addListener(new ChangeListener<String>() {

            public void changed(ObservableValue<? extends String> changed, String oldValue, String newValue) {
                selectedItem = changed.getValue();
            }
        });
        btnAddPatient.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/patientsPage/patient_add.fxml"));
                Parent root1 = null;
                try {
                    root1 = fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initStyle(StageStyle.DECORATED);
                stage.setTitle("Врачи");
                stage.setResizable(false);
                stage.setScene(new Scene(root1, 550, 330));
                Stage stage1 = (Stage) btnAddPatient.getScene().getWindow();
                stage1.close();
                stage.show();
            }
        });

        btnDltPatient.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!selectedItem.isEmpty()) {
                    Pattern pattern = Pattern.compile("^\\d*");
                    Matcher matcher = pattern.matcher(selectedItem);
                    while (matcher.find()) {
                        try {
                            baseData.delete("Patients", "WHERE id = " + matcher.group());
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                    }

                }

            }
        });

    }

    public ArrayList<String> createItemLabel() throws SQLException {
        ArrayList<Patient> patients = baseData.selectPatients("*", "");
        ArrayList<String> strings = new ArrayList<>();
        for (Patient patient :
                patients) {
            strings.add(patient.id + " " + patient.fullName + " " + patient.dateOfAdmission);
        }
        return strings;
    }
}
