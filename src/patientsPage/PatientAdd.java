package patientsPage;

import data.BaseData;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.Doctor;
import models.Patient;

import javax.management.Notification;
import java.awt.*;
import java.io.IOException;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;

public class PatientAdd {
    public TextField tfFullName;
    public DatePicker dateBirthday, dateOfAdmission;
    public Button btnClickAdd;
    public ChoiceBox<String> chbDoctor;
    BaseData baseData = BaseData.getBaseData();
    ArrayList<Doctor> doctors = new ArrayList<>();

    public void initialize() throws SQLException {
        ObservableList<String> items = FXCollections.observableArrayList(getNames());
        chbDoctor.setItems(items);
        btnClickAdd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    if (!tfFullName.getText().isEmpty() && !(dateBirthday.getValue() == null) &&
                            !(dateOfAdmission.getValue() == null) && !(chbDoctor.getValue() ==null)) {
                        doctors = baseData.selectDoctors("*", "WHERE fullname = '" + chbDoctor.getValue() + "'");
                        String dateBirth = dateBirthday.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd", new Locale("ru")));
                        String dateAdmission = dateOfAdmission.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd", new Locale("ru")));
                        baseData.insertPatient(new Patient(0, doctors.get(0).id, tfFullName.getText(), dateBirth, dateAdmission));
                        Stage stage1 = (Stage) btnClickAdd.getScene().getWindow();
                        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/patientsPage/patients_page.fxml"));
                        Parent root1 = null;
                        try {
                            root1 = fxmlLoader.load();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Stage stage = new Stage();
                        stage.initModality(Modality.APPLICATION_MODAL);
                        stage.initStyle(StageStyle.DECORATED);
                        stage.setTitle("Врачи");
                        stage.setResizable(false);
                        stage.setScene(new Scene(root1, 550, 330));
                        stage1.close();
                        stage.show();
                    } else displayMessage();
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        });
    }

    public ArrayList<String> getNames() throws SQLException {
        ArrayList<Doctor> doctors = baseData.selectDoctors("*", "");
        ArrayList<String> strings = new ArrayList<>();
        for (Doctor doctor :
                doctors) {
            strings.add(doctor.fullName);
        }
        return strings;
    }


    public void displayMessage() {

        Stage stage = new Stage();
        Text text = new Text("Введите данные");
        Button button = new Button("Ок");
        button.setLayoutX(85);
        button.setLayoutY(65);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.close();
            }});
        text.setX(60);
        text.setY(50);
        Group group = new Group(text,button);
        Scene scene = new Scene(group, 200, 100);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.show();
    }
}
