package newbornsPage;

import data.BaseData;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.Doctor;
import models.Newborn;
import models.Patient;

import java.io.IOException;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public class NewbornAdd {
    public ChoiceBox<String> chbMother,chbGender;
    public DatePicker dpBirthday;
    public TextField tfFullname,tfHeight,tfWeight;
    public Button btnCkckAdd;

    BaseData baseData = BaseData.getBaseData();
    ArrayList<Patient> mothers = new ArrayList<>();

    public void initialize() throws SQLException {
        ArrayList<String> genders = new ArrayList(Arrays.asList("Муж.","Жен."));
        ObservableList<String> items = FXCollections.observableArrayList(genders);
        chbGender.setItems(items);
        chbMother.setItems(FXCollections.observableArrayList(createItemLabel()));
        btnCkckAdd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {if (!(dpBirthday.getValue() ==null)&&!tfFullname.getText().isEmpty()&&
                        !tfHeight.getText().isEmpty()&&!tfWeight.getText().isEmpty()&&
                        !(chbMother.getValue() ==null) && !(chbGender.getValue() ==null)) {
                    mothers = baseData.selectPatients("*", "WHERE full_name = '" + chbMother.getValue() + "'");
                    String dateBirth = dpBirthday.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd", new Locale("ru")));
                    baseData.insertNewborn(new Newborn(0, mothers.get(0).id, tfFullname.getText(), chbGender.getValue(), dateBirth, Double.parseDouble(tfWeight.getText()), Double.parseDouble(tfHeight.getText())));
                    Stage stage1 = (Stage) btnCkckAdd.getScene().getWindow();
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/newbornsPage/newborns_page.fxml"));
                    Parent root1 = null;
                    try {
                        root1 = fxmlLoader.load();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Stage stage = new Stage();
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.initStyle(StageStyle.DECORATED);
                    stage.setTitle("Новорожденные");
                    stage.setResizable(false);
                    stage.setScene(new Scene(root1, 550, 330));
                    stage1.close();
                    stage.show();
                }else displayMessage();
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        });
    }
    public ArrayList<String> createItemLabel() throws SQLException {
        ArrayList<Patient> patients = baseData.selectPatients("*", "");
        ArrayList<String> strings = new ArrayList<>();
        for (Patient patient :
                patients) {
            strings.add(patient.fullName);
        }
        return strings;
    }
    public void displayMessage() {

        Stage stage = new Stage();
        Text text = new Text("Введите данные");
        Button button = new Button("Ок");
        button.setLayoutX(85);
        button.setLayoutY(65);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.close();
            }});
        text.setX(60);
        text.setY(50);
        Group group = new Group(text,button);
        Scene scene = new Scene(group, 200, 100);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.show();
    }
}
