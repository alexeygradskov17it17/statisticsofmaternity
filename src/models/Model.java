package models;

public class Model {
    public int id;
    public String fullName;

    public Model(int id, String fullName) {
        this.id = id;
        this.fullName = fullName;
    }

}
