package models;

import java.util.Date;

public class Newborn extends Model {
    public int idOfMother;
    public String gender;
    public String birthday;
    public double weight, height;

    public Newborn(int id, int idOfMother,
            String fullName, String gender,
            String birthday, double weight,
            double height) {
        super(id,fullName);
        this.idOfMother = idOfMother;
        this.gender = gender;
        this.birthday = birthday;
        this.weight = weight;
        this.height = height;
    }
}
