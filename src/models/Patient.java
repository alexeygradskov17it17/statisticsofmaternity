package models;

import java.util.Date;

public class Patient extends Model {
    public int idOfDoctor;
    public String birthday, dateOfAdmission;

    public Patient(int id, int idOfDoctor,
            String fullName, String birthday,
            String dateOfAdmission) {
        super(id, fullName);
        this.idOfDoctor = idOfDoctor;
        this.birthday = birthday;
        this.dateOfAdmission = dateOfAdmission;
    }
}
