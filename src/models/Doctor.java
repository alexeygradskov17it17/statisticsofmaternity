package models;

public class Doctor extends Model {
    public String telephone_number;

    public Doctor(int id, String fullName,
                  String telephone_number) {
        super(id, fullName);
        this.telephone_number = telephone_number;
    }
}
