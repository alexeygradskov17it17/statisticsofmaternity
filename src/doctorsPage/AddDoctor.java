package doctorsPage;

import data.BaseData;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.Doctor;

import java.io.IOException;
import java.sql.SQLException;

public class AddDoctor {
    public TextField tfFullName, tfTelephoneNumber;
    public Button btnClickAdd;

    public void initialize() {
        btnClickAdd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    if (!tfFullName.getText().isEmpty() && !tfTelephoneNumber.getText().isEmpty()) {
                        BaseData.getBaseData().insertDoctor(new Doctor(0, tfFullName.getText(), tfTelephoneNumber.getText()));
                        Stage stage1 = (Stage) btnClickAdd.getScene().getWindow();
                        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/doctorsPage/doctor_page.fxml"));
                        Parent root1 = null;
                        try {
                            root1 = fxmlLoader.load();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Stage stage = new Stage();
                        stage.initModality(Modality.APPLICATION_MODAL);
                        stage.initStyle(StageStyle.DECORATED);
                        stage.setTitle("Врачи");
                        stage.setResizable(false);
                        stage.setScene(new Scene(root1, 550, 330));
                        stage1.close();
                        stage.show();
                    }else tfFullName.setPromptText("Введите данные");
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        });
    }
}
