package sample;

import TerminalConst.Constants;
import data.BaseData;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.sql.SQLException;

public class Controller implements Constants {

    public Button btnClckOpenDoctrs, btnClickOpenNewborns, btnClickOpenPatients;


    public void initialize() throws SQLException {
        BaseData baseData = BaseData.getBaseData();

        btnClckOpenDoctrs.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/doctorsPage/doctor_page.fxml"));
                Parent root1 = null;
                try {
                    root1 = fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initStyle(StageStyle.DECORATED);
                stage.setTitle("Врачи");
                stage.setResizable(false);
                stage.setScene(new Scene(root1, 550, 330));
                Stage stage1 = (Stage) btnClckOpenDoctrs.getScene().getWindow();
                stage1.close();
                stage.show();
            }
        });
        btnClickOpenPatients.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/patientsPage/patients_page.fxml"));
                Parent root1 = null;
                try {
                    root1 = fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initStyle(StageStyle.DECORATED);
                stage.setTitle("Пациенты");
                stage.setResizable(false);
                stage.setScene(new Scene(root1, 550, 330));
                Stage stage1 = (Stage) btnClickOpenPatients.getScene().getWindow();
                stage1.close();
                stage.show();
            }
        });
        btnClickOpenNewborns.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/newbornsPage/newborns_page.fxml"));
                Parent root1 = null;
                try {
                    root1 = fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initStyle(StageStyle.DECORATED);
                stage.setTitle("Новорожденные");
                stage.setResizable(false);
                stage.setScene(new Scene(root1, 550, 330));
                Stage stage1 = (Stage) btnClickOpenNewborns.getScene().getWindow();
                stage1.close();
                stage.show();
            }
        });
    }
}
