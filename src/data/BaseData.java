package data;

import com.sun.org.apache.xpath.internal.operations.Mod;
import models.Doctor;
import models.Model;
import models.Newborn;
import models.Patient;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class BaseData {
    static Connection connection;
    static Statement stmt = null;
    public static BaseData baseData;

    public static BaseData getBaseData() {
        if (baseData == null) {
            baseData = new BaseData();
        }
        return baseData;
    }

    private BaseData() {
        connect("jdbc:sqlserver://%1$s;databaseName=%2$s;user=%3$s;password=%4$s;", "DESKTOP-T71AMPQ\\DATA",
                "Maternity",
                "sa",
                "gradskov"
        );

    }


    public void connect(String connectionUrl, String instanceName, String databaseName, String userName, String pass) {
        try {
            String connectionString = String.format(connectionUrl, instanceName, databaseName, userName, pass);
            connection = DriverManager.getConnection(connectionString);
            stmt = connection.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(BaseData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public ArrayList<Doctor> selectDoctors(String columnNames, String predicate) throws SQLException {
        ArrayList<Doctor> doctors = new ArrayList<>();
        ResultSet rs = stmt.executeQuery("SELECT " + columnNames + " FROM Doctors " + predicate);
        while (rs.next()) {
            doctors.add(new Doctor(rs.getInt(1), rs.getString(2), rs.getString(3)));
        }
        return doctors;
    }

    public ArrayList<Patient> selectPatients(String columnNames, String predicate) throws SQLException {
        ArrayList<Patient> patients = new ArrayList<>();
        ResultSet rs = stmt.executeQuery("SELECT " + columnNames + " FROM Patients " + predicate);
        while (rs.next()) {
            patients.add(new Patient(rs.getInt(1), rs.getInt(2),
                    rs.getString(3), rs.getString(4),
                    rs.getString(5)));
        }
        return patients;
    }

    public ArrayList<Newborn> selectNewborns(String columnNames, String predicate) throws SQLException {
        ArrayList<Newborn> newborns = new ArrayList<>();
        ResultSet rs = stmt.executeQuery("SELECT " + columnNames + " FROM Newborns " + predicate);
        while (rs.next()) {
            newborns.add(new Newborn(rs.getInt(1), rs.getInt(2),
                    rs.getString(3), rs.getString(4),
                    rs.getString(5), rs.getDouble(6), rs.getDouble(7)));
        }
        return newborns;
    }

    public void insertDoctor(Doctor doctor) throws SQLException {
        stmt.execute("INSERT INTO Doctors VALUES('" + doctor.fullName + "','" + doctor.telephone_number + "')");
    }

    public void insertPatient(Patient patient) throws SQLException {
        stmt.execute("INSERT INTO Patients VALUES(" + patient.idOfDoctor + ",'" + patient.fullName + "',CONVERT(varchar,'" +
                patient.birthday + "',23)" + ",CONVERT(varchar,'" +
                patient.dateOfAdmission + "',23))");
    }

    public void insertNewborn(Newborn newborn) throws SQLException {
        stmt.execute("INSERT INTO Newborns VALUES(" + newborn.idOfMother + ",'" + newborn.fullName + "',CONVERT(varchar,'" +
                newborn.birthday + "',23),'"+ newborn.gender + "'," + newborn.weight + "," + newborn.height + ")");
    }


    public void delete(String table, String predicate) throws SQLException {
        stmt.execute("DELETE FROM " + table + " " + predicate);
    }
}
