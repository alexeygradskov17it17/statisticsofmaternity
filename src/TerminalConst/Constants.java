package TerminalConst;

public interface Constants {
    String TABLE_DOCTORS_NAME = "Doctors";
    String TABLE_PATIENTS_NAME = "Patients";
    String TABLE_NEWBORN_NAME = "Newborns";
}
